# Esercizi risolti di Fisica I

Questo repository contiene gli esercizi risolti dagli studenti
del corso di Fisica Generale I - LISI

Per modificare o aggiungere una soluzione, apri una sessione Binder
con il link qui sotto:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/bonfus%2Ffisica-generale-i-esercizi/master)

Per scaricare il foglio di calcolo o salvare nel browser un "work in progress" usare i tasi mostrati qui sotto:

![salvare](salvare.png)


## Visualizzare gli esercizi

Per visualizzare in sola lettura uno degli esercizi risolti, usare i link seguenti:

[AltriEserciziSvolti/NOSL_P2.16.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/bonfus/fisica-generale-i-esercizi/-/raw/master/AltriEserciziSvolti/NOSL_P2.16.ipynb)

[AltriEserciziSvolti/NOSL_P2.17.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/bonfus/fisica-generale-i-esercizi/-/raw/master/AltriEserciziSvolti/NOSL_P2.17.ipynb)

[Esercitazione_0/Esercizio1.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/bonfus/fisica-generale-i-esercizi/-/raw/master/Esercitazione_0/Esercizio1.ipynb)

[Esercitazione_0/Esercizio2.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/bonfus/fisica-generale-i-esercizi/-/raw/master/Esercitazione_0/Esercizio2.ipynb)

[EserciziarioDiFisica1_PietroDonatis/P14_E14.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/bonfus/fisica-generale-i-esercizi/-/raw/master/EserciziarioDiFisica1_PietroDonatis/P14_E14.ipynb)

[EserciziarioDiFisica1_PietroDonatis/P15_E4.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/bonfus/fisica-generale-i-esercizi/-/raw/master/EserciziarioDiFisica1_PietroDonatis/P15_E4.ipynb)

[EserciziarioDiFisica1_PietroDonatis/P16_E9.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/bonfus/fisica-generale-i-esercizi/-/raw/master/EserciziarioDiFisica1_PietroDonatis/P16_E9.ipynb)

[EserciziarioDiFisica1_PietroDonatis/P32_E11.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/bonfus/fisica-generale-i-esercizi/-/raw/master/EserciziarioDiFisica1_PietroDonatis/P32_E11.ipynb)

[EserciziarioDiFisica1_PietroDonatis/P32_E7.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/bonfus/fisica-generale-i-esercizi/-/raw/master/EserciziarioDiFisica1_PietroDonatis/P32_E7.ipynb)

[EserciziarioDiFisica1_PietroDonatis/P33_E2.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/bonfus/fisica-generale-i-esercizi/-/raw/master/EserciziarioDiFisica1_PietroDonatis/P33_E2.ipynb)

[EserciziarioDiFisica1_PietroDonatis/P33_E6.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/bonfus/fisica-generale-i-esercizi/-/raw/master/EserciziarioDiFisica1_PietroDonatis/P33_E6.ipynb)

[EserciziarioDiFisica1_PietroDonatis/P33_E7.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/bonfus/fisica-generale-i-esercizi/-/raw/master/EserciziarioDiFisica1_PietroDonatis/P33_E7.ipynb)

[EserciziarioDiFisica1_PietroDonatis/P33_E8.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/bonfus/fisica-generale-i-esercizi/-/raw/master/EserciziarioDiFisica1_PietroDonatis/P33_E8.ipynb)

[EserciziarioDiFisica1_PietroDonatis/P34_E10.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/bonfus/fisica-generale-i-esercizi/-/raw/master/EserciziarioDiFisica1_PietroDonatis/P34_E10.ipynb)

[EserciziarioDiFisica1_PietroDonatis/P34_E15.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/bonfus/fisica-generale-i-esercizi/-/raw/master/EserciziarioDiFisica1_PietroDonatis/P34_E15.ipynb)

[Template.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/bonfus/fisica-generale-i-esercizi/-/raw/master/Template.ipynb)

E' anche possibile caricare i notebook salvati in locale [qui](https://cocalc.com/projects?anonymous=jupyter&session=default).

## Autori

Hanno contribuito al materiale contenuto in questo repository:

* Federico Canali
* Alex Malavolta
* Alessia D'Aniello 
* Matteo Monaco
* Jean Lacroix
* Antonio Signorelli
* Laura Raimondo
* Dmitri Ollari Ischimji
* Nicolò Roffi
* Simon Zeudjio Tchiofo
* Giovanni Schianchi
* Francesco Marchi
* Mauro Passante
